> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS$381

## Chris Thompson

### Assignment 2 Requirements:

*Sub-Heading:*

1. Install ampps
2. Install JDK
3. Install Android Studio and create My First App
4. Provide screenshots of installations
5. Create BitBucket
6. Complete Bitbucket tutorials 
7. Provide git command descriptions

#### README.md file should include the following items:

* Bullet-list items
* 
* 
* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - description here...
2. git status - description here...
3. git add - description here...
4. git commit - description here...
5. git push - description here...
6. git pull - description here...
7. One additional git command (your choice) - description here...

#### Assignment Screenshots:

*Screenshot of main page carousel*:

![AMPPS Installation Screenshot](img/portal_main_page.png)

*Screenshot of failed validation*:

![JDK Installation Screenshot](img/failed_validation.png)

*Screenshot of successful validation*:

![JDK Installation Screenshot](img/passed_validation.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
