> **NOTE:** A README.md file should be placed at the **root of each of your repos directories.**
>


# LIS4381 - Mobile Web Application Development

## Christopher Thompson

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials
    * Provide git command descriptions 
2. [A2 README.md](a2/README.md)
    * TBD
3. [A3 README.md](a3/README.md)
    * TBD
4. [A4 README.md](a4/README.md)
    * Clone assignments starter files
    * Modify cloned repo and develop index.php
    * Develope bootstap Carousel
    * Demostrate data validation cvia Javascript
    * Create Favicon
    8 (http://localhost/repos/lis4381 "link to READ,me file")
5. [A5 README.md](a5/README.md)
    * Data table displays information about pet stores in database
    * Users can add to database by selecting "Add Pet Store" button
    * Newly entered pet store will display is entered info is valid
    * Uses regular expressions to test if entered data is valid

6. [P1 README.md](p1/README.md)
    * TBD
7. [P2 README.md](p2/README.md)
    * Suitably modify meta tags
    * b.Change title, navigation links, and header tags appropriately
    * c.See videos for complete development.
    * d.Turn off client-sidevalidation by commenting out the following code:<script type="text/javascript"                                         src="js/formValidation/formValidation.min.js"></script>
    * e.Add server-sidevalidation and regular expressions--as per the       database entity attribute requirements (and screenshots below):
    