# LIS4381 Mobile Application Development

## Chris Thompson

### Assignment 5

#### Assignment Screenshots:

*Screenshot of Main Page*:

![Data table Screenshot](img/tables.PNG)

* Data table displays information about pet stores in database
* Users can add to database by selecting "Add Pet Store" button
* Newly entered pet store will display is entered info is valid
* Uses regular expressions to test if entered data is valid

*Screenshot of add_petstore_process*:

![Assignment 5 Add Pet Store Process Screenshot](img/error.PNG)

* Screen displays what occurs when you try to visit .../a5/add_petstore_process
Assignment documentation goes here, using Markdown syntax.

Also, global subdir. is missing header.php and footer.php. 
You'll need to create your own files!  :)

